/*
   common.c - common server code routines
   This file is part of the nss-pam-ldapd library.

   Copyright (C) 2006 West Consulting
   Copyright (C) 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013 Arthur de Jong

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301 USA
*/

#include "config.h"

#include <stdio.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>
#include <limits.h>
#include <netdb.h>
#include <string.h>
#include <regex.h>
#include <stdlib.h>
#include <signal.h>

#include "nslcd.h"
#include "common.h"
#include "log.h"
#include "cfg.h"

/* simple wrapper around snptintf() to return non-zero in case
   of any failure (but always keep string 0-terminated) */
int mysnprintf(char *buffer, size_t buflen, const char *format, ...)
{
  int res;
  va_list ap;
  /* do snprintf */
  va_start(ap, format);
  res = vsnprintf(buffer, buflen, format, ap);
  va_end(ap);
  /* NULL-terminate the string just to be on the safe side */
  buffer[buflen - 1] = '\0';
  /* check if the string was completely written */
  return ((res < 0) || (((size_t)res) >= buflen));
}

/* get a name of a signal with a given signal number */
const char *signame(int signum)
{
  switch (signum)
  {
    case SIGHUP:  return "SIGHUP";  /* Hangup detected */
    case SIGINT:  return "SIGINT";  /* Interrupt from keyboard */
    case SIGQUIT: return "SIGQUIT"; /* Quit from keyboard */
    case SIGILL:  return "SIGILL";  /* Illegal Instruction */
    case SIGABRT: return "SIGABRT"; /* Abort signal from abort(3) */
    case SIGFPE:  return "SIGFPE";  /* Floating point exception */
    case SIGKILL: return "SIGKILL"; /* Kill signal */
    case SIGSEGV: return "SIGSEGV"; /* Invalid memory reference */
    case SIGPIPE: return "SIGPIPE"; /* Broken pipe */
    case SIGALRM: return "SIGALRM"; /* Timer signal from alarm(2) */
    case SIGTERM: return "SIGTERM"; /* Termination signal */
    case SIGUSR1: return "SIGUSR1"; /* User-defined signal 1 */
    case SIGUSR2: return "SIGUSR2"; /* User-defined signal 2 */
    case SIGCHLD: return "SIGCHLD"; /* Child stopped or terminated */
    case SIGCONT: return "SIGCONT"; /* Continue if stopped */
    case SIGSTOP: return "SIGSTOP"; /* Stop process */
    case SIGTSTP: return "SIGTSTP"; /* Stop typed at tty */
    case SIGTTIN: return "SIGTTIN"; /* tty input for background process */
    case SIGTTOU: return "SIGTTOU"; /* tty output for background process */
#ifdef SIGBUS
    case SIGBUS:  return "SIGBUS";  /* Bus error */
#endif
#ifdef SIGPOLL
    case SIGPOLL: return "SIGPOLL"; /* Pollable event */
#endif
#ifdef SIGPROF
    case SIGPROF: return "SIGPROF"; /* Profiling timer expired */
#endif
#ifdef SIGSYS
    case SIGSYS:  return "SIGSYS";  /* Bad argument to routine */
#endif
#ifdef SIGTRAP
    case SIGTRAP: return "SIGTRAP"; /* Trace/breakpoint trap */
#endif
#ifdef SIGURG
    case SIGURG:  return "SIGURG";  /* Urgent condition on socket */
#endif
#ifdef SIGVTALRM
    case SIGVTALRM: return "SIGVTALRM"; /* Virtual alarm clock */
#endif
#ifdef SIGXCPU
    case SIGXCPU: return "SIGXCPU"; /* CPU time limit exceeded */
#endif
#ifdef SIGXFSZ
    case SIGXFSZ: return "SIGXFSZ"; /* File size limit exceeded */
#endif
    default:      return "UNKNOWN";
  }
}

/* Checks if the specified name seems to be a valid user or group name. */
int isvalidname(const char *name)
{
  return regexec(&nslcd_cfg->validnames, name, 0, NULL, 0) == 0;
}
