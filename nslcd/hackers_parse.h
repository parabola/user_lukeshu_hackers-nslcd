#ifndef _HACKERS_PARSE_H
#define _HACKERS_PARSE_H

#include <string.h> /* for memset(3) */
#include <stdlib.h> /* for free(3) */
#include <pwd.h> /* for 'struct passwd' */
#include <errno.h> /* for 'errno' */
#include "log.h"

#define ASSERT(expr)                                                 \
	do {                                                         \
		errno = 0;                                           \
		if (!(expr)) {                                       \
			log_log(LOG_ERR, "%s:%d: ASSERT(%s) failed", \
			      __FILE__, __LINE__, #expr);            \
			goto error;                                  \
		}                                                    \
	} while(0)

#define MALLOC(size) REALLOC(NULL, size)

#define REALLOC(ptr, size)                                                 \
	(__extension__ ({                                                  \
		errno = 0;                                                 \
		void *ret = realloc(ptr, size);                            \
		if (ret == (ptr)) {                                        \
			log_log(LOG_ERR, "realloc() failed");              \
			goto error;                                        \
		};                                                         \
		ret;                                                       \
	}))

#define ZERO(var) memset(&(var), 0, sizeof(var))

/* Free+zero a 'struct passwd' */
#define PASSWD_FREE(user) \
	(__extension__ ({ \
		free((user).pw_name); \
		free((user).pw_passwd); \
		free((user).pw_gecos); \
		free((user).pw_dir); \
		free((user).pw_shell); \
		ZERO(user); \
		(user).pw_uid = UID_INVALID; \
	}))

/** Returns 0 on error, or the UID on success.  Only handles "normal
 *  user" UIDs; that is in the range [1000,9999]. */
uid_t	filename2uid(const char *filename);

/** Returns 0 on success, < 0 on failure.  Sets
    userp->pw_passwd. */
int	load_user_password(struct passwd *userp);
/** Returns 0 on success, non-zero on failere.
    May set userp->pw_uid even on failure. */
int	load_user_yaml(const char *filename, struct passwd *userp);

#endif
