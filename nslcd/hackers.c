#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "hackers_watch.h"
#include "log.h"

static
void *
session_worker(void *sess) {
	hackers_session_worker(sess);
	return NULL;
}

struct session *
session_create(const struct nslcd_config *cfg, pthread_t *thread) {
	struct session *session = hackers_session_allocate();
	if (session == NULL) {
		exit(EXIT_FAILURE);
	}

	if (hackers_session_open(session, cfg->yamldir) != 0) {
		exit(EXIT_FAILURE);
	}

	if (pthread_create(thread, NULL, session_worker, (void*)session)) {
		log_log(LOG_ERR, "unable to start hackers worker thread");
		exit(EXIT_FAILURE);
	}

	return session;
}

void
session_check(struct session *sess) {
	/* do nothing */
}

void
session_close(struct session *sess) {
	/*hackers_session_close(sess);*/
}

void
session_refresh(struct session *sess, const struct nslcd_config *cfg) {
	if (hackers_session_open(sess, cfg->yamldir) != 0) {
		exit(EXIT_FAILURE);
	}
}

void
session_messup(struct session *sess) {
	pthread_rwlock_rdlock(&(sess->lock));
}

void
session_cleanup(struct session *sess) {
	pthread_rwlock_unlock(&(sess->lock));
}
