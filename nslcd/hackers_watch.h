#ifndef _HACKERS_WATCH_H
#define _HACKERS_WATCH_H

#include "hackers.h"

struct session * hackers_session_allocate();
int	hackers_session_open(struct session *, const char *yamldir);
int	hackers_session_worker(struct session *);

#endif
