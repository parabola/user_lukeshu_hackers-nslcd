/*
   nslcd.c - name service LDAP connection daemon

   Copyright (C) 2006 West Consulting
   Copyright (C) 2006-2014 Arthur de Jong
   Copyright (C) 2014 Luke Shumaker

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301 USA
*/

#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif /* HAVE_STDINT_H */
#include <sys/types.h>
#include <sys/param.h>
#include <sys/wait.h>
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif /* HAVE_GETOPT_H */
#include <assert.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <grp.h>
#ifdef HAVE_NSS_H
#include <nss.h>
#endif /* HAVE_NSS_H */
#include <pthread.h>
#ifdef HAVE_PTHREAD_NP_H
#include <pthread_np.h>
#endif /* HAVE_PTHREAD_NP_H */
#ifndef HAVE_GETOPT_LONG
#include "compat/getopt_long.h"
#endif /* not HAVE_GETOPT_LONG */
#include <dlfcn.h>
#include <libgen.h>
#include <limits.h>
#include <systemd/sd-daemon.h>

#include "nslcd.h"
#include "log.h"
#include "cfg.h"
#include "common.h"
#include "compat/attrs.h"
#include "compat/getpeercred.h"
#include "compat/socket.h"

/* read timeout is half a second because clients should send their request
   quickly, write timeout is 60 seconds because clients could be taking some
   time to process the results */
#define READ_TIMEOUT 500
#define WRITE_TIMEOUT 60 * 1000

/* buffer sizes for I/O */
#define READBUFFER_MINSIZE 32
#define READBUFFER_MAXSIZE 64
#define WRITEBUFFER_MINSIZE 1024
#define WRITEBUFFER_MAXSIZE 1 * 1024 * 1024

/* the flag to indicate that a signal was received */
static volatile int nslcd_receivedsignal = 0;

/* the server socket used for communication */
static int nslcd_serversocket = -1;

/* thread ids of all running threads */
static pthread_t *nslcd_threads;

/* display version information */
static void display_version(FILE *fp)
{
  fprintf(fp, "%s\n", PACKAGE_STRING);
  fprintf(fp, "Written by Luke Howard and Arthur de Jong.\n\n");
  fprintf(fp, "Copyright (C) 1997-2014 Luke Howard, Arthur de Jong and West Consulting\n"
              "This is free software; see the source for copying conditions.  There is NO\n"
              "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n");
}

/* display usage information */
static void display_usage(FILE *fp, const char *program_name)
{
  fprintf(fp, "Usage: %s [OPTION]...\n", program_name);
  fprintf(fp, "Name Service LDAP connection daemon.\n");
  fprintf(fp, "      --help         display this help and exit\n");
  fprintf(fp, "      --version      output version information and exit\n");
  fprintf(fp, "\n" "Report bugs to <%s>.\n", PACKAGE_BUGREPORT);
}

/* the definition of options for getopt(). see getopt(2) */
static struct option const nslcd_options[] = {
  {"debug",   no_argument, NULL, 'd'},
  {"help",    no_argument, NULL, 'h'},
  {"version", no_argument, NULL, 'V'},
  {NULL,      0,           NULL, 0}
};
#define NSLCD_OPTIONSTRING "cndhV"

/* parse command line options and save settings in struct  */
static void parse_cmdline(int argc, char *argv[])
{
  int optc;
  while ((optc = getopt_long(argc, argv, NSLCD_OPTIONSTRING, nslcd_options, NULL)) != -1)
  {
    switch (optc)
    {
      case 'h': /*     --help         display this help and exit */
        display_usage(stdout, argv[0]);
        exit(EXIT_SUCCESS);
      case 'V': /*     --version      output version information and exit */
        display_version(stdout);
        exit(EXIT_SUCCESS);
      case ':': /* missing required parameter */
      case '?': /* unknown option character or extraneous parameter */
      default:
        fprintf(stderr, "Try '%s --help' for more information.\n", argv[0]);
        exit(EXIT_FAILURE);
    }
  }
  /* check for remaining arguments */
  if (optind < argc)
  {
    fprintf(stderr, "%s: unrecognized option '%s'\n", argv[0], argv[optind]);
    fprintf(stderr, "Try '%s --help' for more information.\n", argv[0]);
    exit(EXIT_FAILURE);
  }
}

/* signal handler for storing information on received signals */
static void sig_handler(int signum)
{
  /* just save the signal to indicate that we're stopping */
  nslcd_receivedsignal = signum;
}

/* do some cleaning up before terminating */
static void exithandler(void)
{
  /* close socket if it's still in use */
  if (nslcd_serversocket >= 0)
  {
    if (close(nslcd_serversocket))
      log_log(LOG_WARNING, "problem closing server socket (ignored):%d",
              nslcd_serversocket);
  }
  /* log exit */
  log_log(LOG_INFO, "version %s bailing out", VERSION);
}

static int get_socket()
{
  int r, fd;
  r = sd_listen_fds(1);
  if (r != 1) {
    if (r < 0)
    {
      errno = -r;
      log_log(LOG_ERR, "failed to aquire sockets from systemd");
    }
    else
      log_log(LOG_ERR, "wrong number of sockets from systemd: "
              "expected %d but got %d", 1, r);
    exit(EXIT_FAILURE);
  }

  fd = SD_LISTEN_FDS_START;

  r = sd_is_socket(fd, AF_UNIX, SOCK_STREAM, 1);
  if (r < 1)
  {
    if (r < 0)
    {
      errno = -r;
      log_log(LOG_ERR, "unable to verify socket type:%d", fd);
    }
    else
      log_log(LOG_ERR, "socket not of the right type:%d", fd);
    exit(EXIT_FAILURE);
  }

  return fd;
}

/* read the version information and action from the stream
   this function returns the read action in location pointer to by action */
static int read_header(TFILE *fp, int32_t *action)
{
  int32_t protocol;
  /* read the protocol version */
  READ_INT32(fp, protocol);
  if (protocol != (int32_t)NSLCD_VERSION)
  {
    log_log(LOG_DEBUG, "invalid nslcd version id: 0x%08x", (unsigned int)protocol);
    return -1;
  }
  /* read the request type */
  READ_INT32(fp, *action);
  return 0;
}

/* read a request message, returns <0 in case of errors,
   this function closes the socket */
static void handleconnection(int sock, struct session *session)
{
  TFILE *fp;
  int32_t action;
  uid_t uid = (uid_t)-1;
  gid_t gid = (gid_t)-1;
  pid_t pid = (pid_t)-1;
  /* log connection */
  if (getpeercred(sock, &uid, &gid, &pid))
    log_log(LOG_DEBUG, "connection from unknown client");
  else
    log_log(LOG_DEBUG, "connection from pid=%d uid=%d gid=%d",
            (int)pid, (int)uid, (int)gid);
  /* create a stream object */
  if ((fp = tio_fdopen(sock, READ_TIMEOUT, WRITE_TIMEOUT,
                       READBUFFER_MINSIZE, READBUFFER_MAXSIZE,
                       WRITEBUFFER_MINSIZE, WRITEBUFFER_MAXSIZE)) == NULL)
  {
    log_log(LOG_WARNING, "cannot create stream for writing");
    (void)close(sock);
    return;
  }
  /* read request */
  if (read_header(fp, &action))
  {
    (void)tio_close(fp);
    return;
  }
  /* handle request */
  session_messup(session);
  dispatch(fp, action, session, uid);
  session_cleanup(session);

  (void)tio_close(fp);
  return;
}

/* try to install signal handler and check result */
static void install_sighandler(int signum, void (*handler) (int))
{
  struct sigaction act;
  memset(&act, 0, sizeof(struct sigaction));
  act.sa_handler = handler;
  sigemptyset(&act.sa_mask);
  act.sa_flags = SA_RESTART | SA_NOCLDSTOP;
  if (sigaction(signum, &act, NULL) != 0)
  {
    log_log(LOG_ERR, "error installing signal handler for '%s'",
            signame(signum));
    exit(EXIT_FAILURE);
  }
}

static void worker_cleanup(void *arg)
{
  struct session *session = (struct session *)arg;
  session_close(session);
}

static void *worker(void *_sess)
{
  struct session *session = _sess;
  int csock;
  int j;
  struct sockaddr_storage addr;
  socklen_t alen;
  fd_set fds;
  /* create a new session */
  /*session = session_create();*/
  /* clean up the session if we're done */
  pthread_cleanup_push(worker_cleanup, session);
  /* start waiting for incoming connections */
  while (1)
  {
    /* perform any maintenance on the session */
    session_check(session);
    /* set up the set of fds to wait on */
    FD_ZERO(&fds);
    FD_SET(nslcd_serversocket, &fds);
    /* wait for a new connection */
    j = select(nslcd_serversocket + 1, &fds, NULL, NULL, NULL);
    /* check result of select() */
    if (j < 0)
    {
      if (errno == EINTR)
        log_log(LOG_DEBUG, "select() failed (ignored)");
      else
        log_log(LOG_ERR, "select() failed");
      continue;
    }
    /* see if our file descriptor is actually ready */
    if (!FD_ISSET(nslcd_serversocket, &fds))
      continue;
    /* wait for a new connection */
    alen = (socklen_t)sizeof(struct sockaddr_storage);
    csock = accept(nslcd_serversocket, (struct sockaddr *)&addr, &alen);
    if (csock < 0)
    {
      if ((errno == EINTR) || (errno == EAGAIN) || (errno == EWOULDBLOCK))
        log_log(LOG_DEBUG, "accept() failed (ignored)");
      else
        log_log(LOG_ERR, "accept() failed");
      continue;
    }
    /* make sure O_NONBLOCK is not inherited */
    if ((j = fcntl(csock, F_GETFL, 0)) < 0)
    {
      log_log(LOG_ERR, "fctnl(F_GETFL) failed");
      if (close(csock))
        log_log(LOG_WARNING, "problem closing socket");
      continue;
    }
    if (fcntl(csock, F_SETFL, j & ~O_NONBLOCK) < 0)
    {
      log_log(LOG_ERR, "fctnl(F_SETFL,~O_NONBLOCK) failed");
      if (close(csock))
        log_log(LOG_WARNING, "problem closing socket");
      continue;
    }
    /* indicate new connection to logging module (generates unique id) */
    log_newsession();
    /* handle the connection */
    handleconnection(csock, session);
    /* indicate end of session in log messages */
    log_clearsession();
  }
  pthread_cleanup_pop(1);
  return NULL;
}

/* function to disable lookups through the associated NSS module to
   avoid lookup loops */
static void disable_nss_module(void)
{
  void *handle;
  char *error;
  char **version_info;
  int *enable_flag;
  /* try to load the NSS module */
#ifdef RTLD_NODELETE
  handle = dlopen(NSS_MODULE_SONAME, RTLD_LAZY | RTLD_NODELETE);
#else /* not RTLD_NODELETE */
  handle = dlopen(NSS_MODULE_SONAME, RTLD_LAZY);
#endif /* RTLD_NODELETE */
  if (handle == NULL)
  {
    log_log(LOG_WARNING, "Warning: NSS " NSS_MODULE_NAME " module not loaded: %s", dlerror());
    return;
  }
  /* clear any existing errors */
  dlerror();
  /* lookup the NSS version if possible */
  version_info = (char **)dlsym(handle, NSS_MODULE_SYM_VERSION);
  error = dlerror();
  if ((version_info != NULL) && (error == NULL))
    log_log(LOG_DEBUG, "NSS " NSS_MODULE_NAME " %s %s", version_info[0], version_info[1]);
  else
    log_log(LOG_WARNING, "Warning: " NSS_MODULE_NAME " version missing: %s", error);
  /* clear any existing errors */
  dlerror();
  /* try to look up the flag */
  enable_flag = (int *)dlsym(handle, NSS_MODULE_SYM_ENABLELOOKUPS);
  error = dlerror();
  if ((enable_flag == NULL) || (error != NULL))
  {
    log_log(LOG_WARNING, "Warning: %s (probably older NSS module loaded)",
            error);
    /* fall back to changing the way host lookup is done */
#ifdef HAVE___NSS_CONFIGURE_LOOKUP
    if (__nss_configure_lookup("hosts", "files dns"))
      log_log(LOG_ERR, "unable to override hosts lookup method");
#endif /* HAVE___NSS_CONFIGURE_LOOKUP */
    dlclose(handle);
    return;
  }
  /* disable the module */
  *enable_flag = 0;
#ifdef RTLD_NODELETE
  /* only close the handle if RTLD_NODELETE was used */
  dlclose(handle);
#endif /* RTLD_NODELETE */
}

/* the main program... */
int main(int argc, char *argv[])
{
  int i;
  sigset_t signalmask, oldmask;

  /* parse the command line */
  parse_cmdline(argc, argv);

  /* disable the associated NSS module for this process */
  disable_nss_module();

  /* read configuration file */
  cfg_init(NSLCD_CONF_PATH);

  /* log start */
  log_log(LOG_INFO, "version %s starting", VERSION);
  /* install handler to close stuff off on exit and log notice */
  if (atexit(exithandler))
  {
    log_log(LOG_ERR, "atexit() failed");
    exit(EXIT_FAILURE);
  }
  /* get socket */
  nslcd_serversocket = get_socket();
  /* start subprocess to do invalidating if reconnect_invalidate is set */
  for (i = 0; i < LM_NONE; i++)
    if (nslcd_cfg->reconnect_invalidate[i])
      break;
  if (i < LM_NONE)
    invalidator_start();

  /* block all these signals so our worker threads won't handle them */
  sigemptyset(&signalmask);
  sigaddset(&signalmask, SIGHUP);
  sigaddset(&signalmask, SIGINT);
  sigaddset(&signalmask, SIGQUIT);
  sigaddset(&signalmask, SIGABRT);
  sigaddset(&signalmask, SIGPIPE);
  sigaddset(&signalmask, SIGTERM);
  sigaddset(&signalmask, SIGUSR1);
  sigaddset(&signalmask, SIGUSR2);
  pthread_sigmask(SIG_BLOCK, &signalmask, &oldmask);
  /* start worker threads */
  log_log(LOG_INFO, "accepting connections");
  nslcd_threads = malloc(nslcd_cfg->threads * sizeof(pthread_t));
  if (nslcd_threads == NULL)
  {
    log_log(LOG_CRIT, "main(): malloc() failed to allocate memory");
    exit(EXIT_FAILURE);
  }
  struct session *session = session_create(nslcd_cfg, &nslcd_threads[0]);
  for (i = 1; i < nslcd_cfg->threads; i++)
  {
    if (pthread_create(&nslcd_threads[i], NULL, worker, (void*)session))
    {
      log_log(LOG_ERR, "unable to start worker thread %d", i);
      exit(EXIT_FAILURE);
    }
  }
  pthread_sigmask(SIG_SETMASK, &oldmask, NULL);
  /* install signalhandlers for some signals */
  install_sighandler(SIGHUP, sig_handler);
  install_sighandler(SIGINT, sig_handler);
  install_sighandler(SIGQUIT, sig_handler);
  install_sighandler(SIGABRT, sig_handler);
  install_sighandler(SIGPIPE, SIG_IGN);
  install_sighandler(SIGTERM, sig_handler);
  install_sighandler(SIGUSR1, sig_handler);
  install_sighandler(SIGUSR2, SIG_IGN);
  sd_notify(0, "READY=1");
  /* wait until we received a signal */
  while ((nslcd_receivedsignal == 0) || (nslcd_receivedsignal == SIGUSR1))
  {
    sleep(INT_MAX); /* sleep as long as we can or until we receive a signal */
    if (nslcd_receivedsignal == SIGUSR1)
    {
      log_log(LOG_INFO, "caught signal %s (%d), refresh retries",
              signame(nslcd_receivedsignal), nslcd_receivedsignal);
      session_refresh(session, nslcd_cfg);
      nslcd_receivedsignal = 0;
    }
  }
  sd_notify(0, "STOPPING=1");
  /* print something about received signal */
  log_log(LOG_INFO, "caught signal %s (%d), shutting down",
          signame(nslcd_receivedsignal), nslcd_receivedsignal);
  /* cancel all running threads */
  for (i = 0; i < nslcd_cfg->threads; i++)
    if (pthread_cancel(nslcd_threads[i]))
      log_log(LOG_WARNING, "failed to stop thread %d (ignored)", i);
  /* close server socket to trigger failures in threads waiting on accept() */
  close(nslcd_serversocket);
  nslcd_serversocket = -1;
  /* if we can, wait a few seconds for the threads to finish */
#ifdef HAVE_PTHREAD_TIMEDJOIN_NP
  struct timespec ts = {
    .tv_sec = time(NULL) + 3,
    .tv_nsec = 0,
  };
#endif /* HAVE_PTHREAD_TIMEDJOIN_NP */
  for (i = 0; i < nslcd_cfg->threads; i++)
  {
#ifdef HAVE_PTHREAD_TIMEDJOIN_NP
    pthread_timedjoin_np(nslcd_threads[i], NULL, &ts);
#endif /* HAVE_PTHREAD_TIMEDJOIN_NP */
    if (pthread_kill(nslcd_threads[i], 0) == 0)
      log_log(LOG_ERR, "thread %d is still running, shutting down anyway", i);
  }
  /* we're done */
  return EXIT_FAILURE;
}
