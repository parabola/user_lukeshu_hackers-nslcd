#ifndef _HACKERS_H
#define _HACKERS_H

#include <pthread.h>
#include <pwd.h>
#include "cfg.h"

#define GID_INVALID ((gid_t)-1)
#define UID_INVALID ((uid_t)-1)

struct session {
	pthread_rwlock_t lock;
	size_t cnt;
	struct passwd *users;
	/* The following are only for writers */
	char *yamldir;
	int *in_user_wds;
	int in_fd;
	int in_wd_home;
	int in_wd_yaml;
};

/*struct session *session_create(const nslcd_config *);*/ /* create */
struct session *session_create(const struct nslcd_config *, pthread_t *);
void session_check(struct session *); /* maintain */
void session_close(struct session *); /* destroy */
void session_refresh(struct session *, const struct nslcd_config *); /* refresh */

void session_messup(struct session *); /* before dispatch */
void session_cleanup(struct session *); /* after dispatch */

#endif
