/*
   cfg.h - definition of configuration information
   This file contains parts that were part of the nss_ldap
   library which has been forked into the nss-pam-ldapd library.

   Copyright (C) 1997-2005 Luke Howard
   Copyright (C) 2007 West Consulting
   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012, 2013 Arthur de Jong

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301 USA
*/

#ifndef NSLCD__CFG_H
#define NSLCD__CFG_H

#include <regex.h>
#include <stdbool.h>

#include "compat/attrs.h"
#include "common/set.h"

/* selectors for different maps */
enum nss_map_selector {
  LM_ALIASES,
  LM_ETHERS,
  LM_GROUP,
  LM_HOSTS,
  LM_NETGROUP,
  LM_NETWORKS,
  LM_PASSWD,
  LM_PROTOCOLS,
  LM_RPC,
  LM_SERVICES,
  LM_SHADOW,
  LM_NFSIDMAP, /* only used for cache invalidation */
  LM_NONE
};

struct nslcd_config {
  char *yamldir;

  int threads;    /* the number of threads to start */

  SET *nss_initgroups_ignoreusers;  /* the users for which no initgroups() searches should be done */
  uid_t nss_min_uid;  /* minimum uid for users retrieved */
  bool nss_nested_groups; /* whether to expand nested groups */
  regex_t validnames; /* the regular expression to determine valid names */
  char *validnames_str; /* string version of validnames regexp */
  bool ignorecase; /* whether or not case should be ignored in lookups */
  char *pam_password_prohibit_message;   /* whether password changing should be denied and user prompted with this message */
  char reconnect_invalidate[LM_NONE];  /* set to 1 if the corresponding map should be invalidated */
};

/* this is a pointer to the global configuration, it should be available
   and populated after cfg_init() is called */
extern struct nslcd_config *nslcd_cfg;

/* Initialize the configuration in nslcd_cfg. This method will read the
   default configuration file and call exit() if an error occurs. */
void cfg_init(const char *fname);

#endif /* NSLCD__CFG_H */
