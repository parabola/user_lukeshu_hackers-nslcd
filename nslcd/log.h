/*
   log.h - definitions of logging funtions

   Copyright (C) 2002, 2003, 2007, 2008, 2010, 2011, 2012, 2013 Arthur de Jong

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301 USA
*/


#ifndef NSLCD__LOG_H
#define NSLCD__LOG_H 1

#include "compat/attrs.h"

#define	LOG_EMERG	0
#define	LOG_ALERT	1
#define LOG_CRIT	2
#define LOG_ERR	3
#define LOG_WARNING	4
#define LOG_NOTICE	5
#define LOG_INFO	6
#define LOG_DEBUG	7

/* indicate that a session id should be included in the output
   and set it to a new value */
void log_newsession(void);

/* indicate that we should clear any session identifiers set by
   log_newsession */
void log_clearsession(void);

/* indicate that a request identifier should be included in the output
   from this point on, until log_newsession() is called */
void log_setrequest(const char *format, ...)
  LIKE_PRINTF(1, 2);

/* log the given message using the configured logging method */
void log_log(int pri, const char *format, ...)
  LIKE_PRINTF(2, 3);

#endif /* not NSLCD__LOG_H */
