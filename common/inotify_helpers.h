/* inotify_helper.h - Simple iteration for inotify events
 *
 * Copyright (C) 2014 Luke Shumaker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _INOTIFY_HELPER_H
#define _INOTIFY_HELPER_H

#include <sys/inotify.h>	/* for 'struct inotify_event' */
#include <sys/types.h>	/* for ssize_t, uint32_t */
#include <limits.h>	/* for NAME_MAX */

char *inotify_mask2str(uint32_t mask);
int inotify_print_event(struct inotify_event *event);

struct inotify_buffer {
	ssize_t len;
	ssize_t pos;
	char dat[sizeof(struct inotify_event)+NAME_MAX+1];
};

struct inotify_event *inotify_next_event_r(int fd, struct inotify_buffer *buf);
struct inotify_event *inotify_next_event(int fd);

#define INOTIFY_ITERATOR(FD, EVENT) \
	struct inotify_event *EVENT = inotify_next_event(FD); \
	EVENT != NULL; \
	EVENT = inotify_next_event(FD)

#define INOTIFY_ITERATOR_R(FD, EVENT, BUF) \
	struct inotify_event *EVENT = inotify_next_event_r(FD, BUF); \
	EVENT != NULL; \
	EVENT = inotify_next_event_r(FD, BUF)

#endif
